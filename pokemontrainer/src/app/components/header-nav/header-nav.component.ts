import { Component} from '@angular/core';
import {TrainerService} from '../../services/trainer.service'
import {Observable} from 'rxjs'

@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.css']
})
export class HeaderNavComponent {

  constructor(private trainerService: TrainerService) { }

  get trainerName(): Observable<string>{
    return this.trainerService.getTrainer();
  }
}
