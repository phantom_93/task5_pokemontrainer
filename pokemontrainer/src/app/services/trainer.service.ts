import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

const STORAGE_ITEM = 'poketrainer'; 
@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  // observable
  private trainer$: BehaviorSubject<string> = new BehaviorSubject('');

  constructor() { 
    const savedTrainer = localStorage.getItem(STORAGE_ITEM);
    // if the trainer is already saved/ logged in, return that trainer's name 
    if(savedTrainer)
      this.trainer$.next(savedTrainer);
  }


  // functions  
  hasTrainer(): boolean{
    return Boolean(this.trainer$.value);
  }

  // return as observable string 
  getTrainer(): Observable<string>{
     return this.trainer$.asObservable();
  }

  // save the name of trainer in the local storage. 
  register(name:string): void{
    this.trainer$.next(name); 
    localStorage.setItem(STORAGE_ITEM, name);
  }

  // remove the saved name when log out of the game 
  logout(): void {
    localStorage.removeItem(STORAGE_ITEM);
    this.trainer$.next('');
  }
}
