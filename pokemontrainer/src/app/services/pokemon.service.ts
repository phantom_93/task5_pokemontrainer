import { Injectable } from '@angular/core';
import {Pokemon} from '../models/pokemon.model'
import { BehaviorSubject, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  public pokemons$: BehaviorSubject<Pokemon[]> = new BehaviorSubject([]); //All pokemons
  public pokemonDetail$: BehaviorSubject<Pokemon> = new BehaviorSubject(null); //CLicked for details
  constructor(private http: HttpClient) { }

  private getIdAndImage(url: string): any {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return {id, url: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }
getImg(name: string){
  return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ name }.png`
}

//Get all pokemons for the catalogue
getPokemon(n=10): void{
    console.log('Running pokemon service')
    this.http.get<Pokemon[]>( `https://pokeapi.co/api/v2/pokemon?limit=${n}` )
      .pipe(map( (response: any) => response.results.map( pokemon => {
            return {
              ...pokemon,
              ...this.getIdAndImage( pokemon.url )
            };
          }
        )))
      .subscribe( (pokemon: Pokemon[]) => {
        this.pokemons$.next( pokemon );
    } );
}

//Get details for each pokemon
getDetail(name){
  //console.log('Getting pokemon details for ' + name)
  this.pokemonDetail$.next(null);
  this.http.get(`https://pokeapi.co/api/v2/pokemon/${name}`).subscribe( (details: Pokemon) => {
    this.pokemonDetail$.next( details );
  });
}
}
