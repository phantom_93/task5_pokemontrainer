// import the pokemon sprite from pokemon-sprite 
import { PokemonSprite } from "./pokemon-sprite";

// property that a pokemon have 
export interface Pokemon{
    id?: string; 
    name: string; 
    url: string; 
    abilities: any[];
    forms?: any; 
    stats?: any; 
    sprites?: PokemonSprite; 
    moves?: any[]; 
    baseExperience?: number; 
    height?: number; 
    weight?: number; 
    types?: number; 

}